package com.example.informate_19;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;//La conexion de los servicios de firebase se hace directamente en la pagina de firebase vinculandolo con el nombre del proyecto

public class MainActivity extends AppCompatActivity {
    private String usuario="";
    private String contraseña="";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView registros =(TextView) findViewById(R.id.registroLogin);//Declaracion de cada uno de los botones y cuadros de texto
        ImageButton Informate=(ImageButton)findViewById(R.id.Btninfo);//Declaracion de cada uno de los botones y cuadros de texto
        Button Entrar=(Button)findViewById(R.id.btnLogin);//Declaracion de cada uno de los botones y cuadros de texto
        final EditText Usuario=(EditText)findViewById(R.id.edtusername);//Declaracion de cada uno de los botones y cuadros de texto
        final EditText Contraseña=(EditText)findViewById(R.id.edtContra);//Declaracion de cada uno de los botones y cuadros de texto
        mAuth=FirebaseAuth.getInstance();//Conexion a firebase
        Informate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent o=new Intent(getApplicationContext(),Informacion.class);//Cambio de activity
                startActivity(o);
            }
        });

        registros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),Registro.class);//Cambio de activity
                startActivity(i);
            }
        });
        Entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usuario=Usuario.getText().toString();//Verificacion de datos llenos
                contraseña=Contraseña.getText().toString();//Verificacion de datos llenos
                if (!usuario.isEmpty()&&!contraseña.isEmpty()){//Si los campos estan llenos realizar un proceso
                    LoginUsu();
                }
                else {
                    Toast.makeText(MainActivity.this,"Campos vacios",Toast.LENGTH_SHORT).show();//Si los capos estan vacios pide llenar los datos
                }
            }
        });
    }
    private void LoginUsu(){
        mAuth.signInWithEmailAndPassword(usuario,contraseña).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {//Verificacion de datos llenos y autenticidad en la base de datos
                if (task.isSuccessful()){
                    startActivity(new Intent(MainActivity.this,Menu.class));//Cambio de activity si se concuerda con los datos de la base de datos
                    finish();
                    Toast.makeText(MainActivity.this,"Bienvenido"+usuario ,Toast.LENGTH_LONG).show();//Si entra manda un mensaje del usuario que accedio
                }
                else {
                    Toast.makeText(MainActivity.this,"El correo electrónico y la contraseña que ingresaste no coinciden con nuestros registros. Por favor, revisa e inténtalo de nuevo.",Toast.LENGTH_LONG).show();
                }//Verifica que el usuario esten correctos y si no pide volver a Verificarlos
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth.getCurrentUser()!=null){
            startActivity(new Intent(MainActivity.this,Menu.class));

            finish();
        }
    }
}