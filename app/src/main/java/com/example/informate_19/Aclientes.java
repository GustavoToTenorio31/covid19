package com.example.informate_19;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class Aclientes extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private Button CerrarSes;
    private ImageButton Enviar;
    public EditText Mensaje;
    private RecyclerView Contenido;


    private List<Msg> ListMensaje;
    private Adaptar mAdaptar;//Se manda a llamar la clase en donde se adaptan los mensajes


    private void setComponentes(){
        Contenido=findViewById(R.id.recyclerView2);//Declaracion de cada uno de los botones y cuadros de texto
        Enviar=findViewById(R.id.btnsend);//Declaracion de cada uno de los botones y cuadros de texto
        Mensaje=findViewById(R.id.rvMensajes);//Declaracion de cada uno de los botones y cuadros de texto

        ListMensaje=new ArrayList<>();
        mAdaptar =new Adaptar(ListMensaje);//Se manda a llamar la lista de los mensajes de la clase adaptar

        Contenido.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        Contenido.setAdapter(mAdaptar);
        Contenido.setHasFixedSize(true);//Verifica que contenga los campos llenos para poder enviar
        FirebaseFirestore.getInstance().collection("Chat").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                assert queryDocumentSnapshots != null;
                for (DocumentChange mDocumentChange:queryDocumentSnapshots.getDocumentChanges()){
                if (mDocumentChange.getType()==DocumentChange.Type.ADDED){
                    ListMensaje.add(mDocumentChange.getDocument().toObject(Msg.class));
                    mAdaptar.notifyDataSetChanged();//Muestra los contenidos de los procesos y del tamaño de las listas
                    Contenido.smoothScrollToPosition(ListMensaje.size());
                }
            }
            }
        });
        Enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Mensaje.length()==0)//No permite que se envien mensajes en blanco
                    return;
                Msg mMsg=new Msg();//Se mandan a llamar las vistas de los mensajes de la clase de los Mensajes
                mMsg.setMensaje(Mensaje.getText().toString());
                FirebaseFirestore.getInstance().collection("Chat").add(mMsg);//se agregan los mensajes en tiempo real para que se brinde atrencion
                Mensaje.setText(" ");//Mencion del campo de mensajes
                if (Mensaje==null)
                    Toast.makeText(Aclientes.this,"Error en conexión",Toast.LENGTH_LONG).show();//En dado caso de no tener una conexion manda un error
                return;
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aclientes);
        Button CerrarSes=(Button)findViewById(R.id.btncerses);
        mAuth=FirebaseAuth.getInstance();

        CerrarSes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                startActivity(new Intent(Aclientes.this,MainActivity.class));//Finalizacion de cesion del usuario

                Toast.makeText(Aclientes.this,"Hasta la proxima",Toast.LENGTH_LONG).show();
            }
        });

    }
}