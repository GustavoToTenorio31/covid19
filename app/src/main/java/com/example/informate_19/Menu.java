package com.example.informate_19;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class Menu extends AppCompatActivity {
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Button AbrirInfo=(Button)findViewById(R.id.btnonfo);//Declaracion de cada uno de los botones
        Button Finalizar=(Button)findViewById(R.id.btnfin);//Declaracion de cada uno de los botones y cuadros de texto
        Button AbrirChat=(Button)findViewById(R.id.btnchat);//Declaracion de cada uno de los botones y cuadros de texto
        Button Estadistica=(Button)findViewById(R.id.btnEsta);//Declaracion de cada uno de los botones y cuadros de texto
        mAuth=FirebaseAuth.getInstance();//Hace referencia a la conexion

        AbrirInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this,Informacion.class));//Se realiza el cambio de activity

            }
        });
        Finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();//Finaliza la entrada del usuario
                startActivity(new Intent(Menu.this,MainActivity.class));//Regresa a la pagina de inicio
                finish();
                Toast.makeText(Menu.this,"Hasta la proxima",Toast.LENGTH_LONG).show();//Mensaje de despedida
            }
        });
        AbrirChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this,Aclientes.class));//Cambio de activity

            }
        });
        Estadistica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this,Mensajes.class));//Cambio de activity
            }
        });
    }
}