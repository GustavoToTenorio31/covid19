package com.example.informate_19;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adaptar extends RecyclerView.Adapter<Adaptar.MensajeHolder> {//Se crea una clase que contenga la extencion del formato del chat
    private List<Msg>ListMensajes;

    public Adaptar(List<Msg> ListMensajes) {
        this.ListMensajes = ListMensajes;
    }

    @NonNull
    @Override
    public MensajeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View mView= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mensajechat,viewGroup,false);//Se pide un proceso mediante una vista
        return new MensajeHolder(mView);//Se manda a llamar el proceso mediante un argumento
    }

    @Override
    public void onBindViewHolder(@NonNull MensajeHolder mensajeHolder, int i) {

        mensajeHolder.txtVmensaje.setText(ListMensajes.get(i).getMensaje());//Se almacena la informacion que debe de contener

    }

    @Override
    public int getItemCount() {
        return ListMensajes.size();
    }

    class MensajeHolder extends RecyclerView.ViewHolder {
        private TextView txtVmensaje;
        public MensajeHolder(@NonNull View itemView) {
            super(itemView);
            txtVmensaje=itemView.findViewById(R.id.txtVmensaje);//se inicializa el proceso que debe de realizar
        }

    }
}
