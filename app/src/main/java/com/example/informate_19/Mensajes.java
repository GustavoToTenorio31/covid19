package com.example.informate_19;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

public class Mensajes extends AppCompatActivity {
    String url="https://news.google.com/covid19/map?hl=es-419&mid=%2Fm%2F0b90_r&gl=MX&ceid=MX%3Aes-419";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensajes);
        WebView link=(WebView)findViewById(R.id.wbvEsta);//Se manda a llamar un link en el cual se mostrara informacion requerida
        link.loadUrl(url);//Se establece el link

    }
}