package com.example.informate_19;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class Registro extends AppCompatActivity {
    private TextView Usuario;
    private TextView Contraseña;
    private Button Acceder;

    private String usuario="";
    private String contraseña="";
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        mAuth=FirebaseAuth.getInstance();//se establece la conexion con firebase para poder verificar el usuario
        mDatabase=FirebaseDatabase.getInstance().getReference();//Hace referencia a la conexion de Firebase
        final TextView Usuario =(TextView) findViewById(R.id.nombreUsuario);//Declaracion de cada uno de los botones y cuadros de texto
        final TextView Contraseña=(TextView)findViewById(R.id.contra);//Declaracion de cada uno de los botones y cuadros de texto
        Button Acceder=(Button)findViewById(R.id.Crear);//Declaracion de cada uno de los botones y cuadros de texto
        TextView Inicio=(TextView) findViewById(R.id.backreg);//Declaracion de cada uno de los botones y cuadros de texto

        Acceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            usuario=Usuario.getText().toString();//Declaracion de cuadros de texto
            contraseña=Contraseña.getText().toString();//Declaracion de cuadros de texto
            if (!usuario.isEmpty()&&!contraseña.isEmpty()){//Verifica que los campos esten llenos
                if (contraseña.length()>=6){//Define el tamaño de la contraseña
                    registrarUsuario();
                }
                else{
                    Toast.makeText(Registro.this,"Ingresa una contraseña de almenos 6 caracteres",Toast.LENGTH_LONG).show();//Si la contraseña no cumple con el tamaño requeridao manda el mensaje
                }

            }
            else{
                Toast.makeText(Registro.this,"Campos vacios",Toast.LENGTH_LONG).show();//En caso de que no se llenen los campos mandara un mensaje
            }

            }
        });
        Inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(Registro.this,MainActivity.class));//Manda al activity de registro de usuarios
               finish();
            }
        });
    }

    private void registrarUsuario(){
    mAuth.createUserWithEmailAndPassword(usuario,contraseña).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {//Verifica que esten llenos los campos de creacion de usuario
        if (task.isSuccessful()){
            Map<String,Object>map=new HashMap<>();
            map.put("Usuario", usuario);//Campo lleno
            map.put("Contraseña",contraseña);//campo lleno
            String id=mAuth.getCurrentUser().getUid();
            mDatabase.child("Usuario").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task2) {//Marca la creacion de usuario que sea correcta
                if (task2.isSuccessful()){
                    startActivity(new Intent(Registro.this,MainActivity.class));//En caso de que el proceso sea correcto se autoriza el acceso
                    finish();
                    Toast.makeText(Registro.this,"Bienvenido" + usuario,Toast.LENGTH_LONG).show();//Manda un mensaje de bienvenida al momento de entrar
                }
                else {
                    Toast.makeText(Registro.this,"No se pudieron crear los datos",Toast.LENGTH_LONG).show();//En caso de tener algun error manda un mensaje para volver a intentarlo
                }
                }
            });
        }
        else {
        Toast.makeText(Registro.this,"Usuario no registrado",Toast.LENGTH_LONG).show();//En caso de no cumplir con los requisitos manda error
        }
        }
    });
    }
}